import "../styles/App.css";
import "../styles/Header.css";

import "../styles/Map.css";
import "../styles/Footer.css";
import "../styles/ContainerMiddle.css";
import "../styles/TextHeaderTop.css";
import Header from "../components/Header";
import Map from "../components/Map";
import Footer from "../components/Footer";
import TextHeaderTop from "./TextHeaderTop";
import Meteo from "../components/Meteo";

function App() {
  return (
    <div className="App">
      <div>
        <Header />
        <TextHeaderTop />
      </div>
      <div id="ContainerMiddle">
        <Map />
      </div>

      <Footer />
    </div>
  );
}

export default App;
