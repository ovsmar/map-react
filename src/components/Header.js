import Logo from "../assets/logo_simplon.png";
import Photo from "../assets/photo.jpg";
const Header = () => {
  return (
    <div id="containerHeader">
      <div id="un">
        <img src={Logo} alt="Logo Simplon" id="logoSimplon" />
      </div>
      <div id="deux">
        <div id="deuxUn">
          <h2>Simplon.co en Occitanie</h2>
          <p>
            Simplon.co est un réseau de Fabriques solidaires et inclusives qui
            proposent des formations gratuites aux métiers techniques du
            numérique en France et à l’étranger
          </p>
          <button id="btnHeader">Formations ouvertes</button>
        </div>
        <div id="deuxDeux">
          <img src={Photo} alt="Photo Simplon" id="PhotoSimplon" />
        </div>
      </div>
    </div>
  );
};

export default Header;
