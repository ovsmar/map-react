import React, { useState } from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import Meteo from "../components/Meteo.js";
import positions from "../fabriques.json";

const Map = () => {
  console.log(positions);
  const [data, setData] = useState(null);
  const [i, seti] = useState([0, 0]);
  var L = window.L;

  navigator.geolocation.getCurrentPosition(function (position) {
    let a = position.coords.latitude;
    let b = position.coords.longitude;
    console.log(a, b);
    seti([a, b]);
  });

  var iconPerson = new L.Icon({
    iconUrl:
      "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png",

    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
  });
  return (
    <div className="container">
      <div id="mapRight">
        <p>
          <h3>{data ? "Ville:" + data.name : ""}</h3>
          <br></br>
          <h6>{data ? "L'adresse:" + data.adresse : ""}</h6>
          <br></br>
          <h3>{data ? "Formation:" + data.formation : ""}</h3>
        </p>
      </div>

      <div id="map">
        <MapContainer center={[43.6487851, 2.3435684]} zoom={7}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />

          {positions.map((ville) => (
            <Marker
              eventHandlers={{
                click: () => {
                  setData(ville);
                },
              }}
              position={[ville.longitude, ville.latitude]}
              key={ville.name}
            >
              <Popup>
                {ville.name}
                {/* <br></br>
                {ville.formation}
                <br></br>
                {ville.adresse} */}
              </Popup>
              ￼￼
            </Marker>
          ))}
          <Marker icon={iconPerson} position={i}>
            <Popup>
              <Meteo />
            </Popup>
          </Marker>
        </MapContainer>
      </div>
    </div>
  );
};

export default Map;
