import ReactWeather, { useOpenWeather } from "react-open-weather";

const Meteo = () => {
  const { data, isLoading, errorMessage } = useOpenWeather({
    key: "f5f91947fd3a1701d7391fcb32e5fec8",
    lat: 43.6487851,
    lon: 2.3435684,
    lang: "fr",
    unit: "metric", // values are (metric, standard, imperial)
  });
  return (
    <ReactWeather
      isLoading={isLoading}
      errorMessage={errorMessage}
      data={data}
      lang="fr"
      locationLabel="Narbonne"
      unitsLabels={{ temperature: "C", windSpeed: "Km/h" }}
      showForecast
    />
  );
};

export default Meteo;
